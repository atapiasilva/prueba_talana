from django import forms


class ImageUploadForm(forms.Form):
    image = forms.ImageField()
    nickname = forms.CharField(max_length=250)
    name_pets = forms.CharField(max_length=250)