from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Post(models.Model):
    model_pic = models.ImageField(upload_to = 'pic_folder/', default = 'pic_folder/None/no-img.jpg')
    nickname = models.CharField(max_length=250)
    name_pets = models.CharField(max_length=250)
    likes = models.IntegerField(default=0)

    def __str__(self):
          return self.nickname


