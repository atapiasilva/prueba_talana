from django.shortcuts import render, redirect, get_object_or_404

from django.http import HttpResponse
from .models import Post
from .forms import ImageUploadForm
from django.db.models import F

# Create your views here.

def upload(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            model_pic = form.cleaned_data['image']
            nickname = form.cleaned_data['nickname']
            name_pets = form.cleaned_data['name_pets']
            m = Post.objects.get_or_create(model_pic=model_pic, nickname=nickname,name_pets=name_pets)
            return redirect('/')
    elif request.method == 'GET':
          return render(request,'upload.html')     

def home(request):
    example = Post.objects.all()
    context = {
        'example': example,

    }
    return render(request,'home.html', context)

def like_post(request, nickname):
     print(nickname)
     #post = get_object_or_404(Post, nickname=nickname)
     Post.objects.filter(nickname=nickname).update(likes=F('likes') + 1)
     return redirect('/')

   #
